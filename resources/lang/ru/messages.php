<?php

return [

    'page_not_found'            => 'Страница не найдена',
    'upload_image_label'        => 'Выберите изображение для загрузки',
    'upload_drag_area'          => 'Выберите файл или перенесите в эту область',
    'published'                 => 'Опубликовано',
    'not_published'             => 'Не опубликовано',
    'deleted'                   => 'Удалено',
    'new'                       => 'Новый',
    'rejected'                  => 'Отменено',
    'wait_approve'              => 'Ожидает подтверждения',
    'Search'                    => 'Искать',
];