<?php

return [

    'go_to_homepage' => 'Перейти на главную',
    'registration_text' => 'Зарегистрироваться',
    'login_text' => 'Войти',
    'password_recovery' => 'Восстановить пароль',
    'profile' => 'Мой профиль',
    'next' => 'Продолжить',
    'register_text' => 'Регистрация',

];
