import Vue from 'vue'
import Router from 'vue-router'
import Home from '../pages/Home'
import Monitoring from '../pages/Monitoring'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
        },
        {
            path: '/monitoring',
            name: 'monitoring',
            component: Monitoring,
        }
    ]
})
