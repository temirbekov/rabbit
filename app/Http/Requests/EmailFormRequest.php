<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmailFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'to' => 'required|string|max:250',
                        'from' => 'required|string|max:250',
                        'subject' => 'required|string|max:25',
                        'message' => 'required|string|max:1000',
                    ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [

                    ];
                }
            default:break;
        }
    }

}
