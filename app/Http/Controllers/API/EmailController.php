<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiBaseController;
use App\Http\Requests\EmailFormRequest;
use App\Repositories\EmailRepository;

class EmailController extends ApiBaseController
{

    public function store(EmailFormRequest $request, EmailRepository $emailRepository) {
        $data = $request->only(['to','from','subject','message']);

        //Создаем задачу в БД
        $model = $emailRepository->store($data);

        //Отправляем в очередь
        $emailRepository->dispatch($model);

        return $this->sendResponse(
            true, ['status'=>'success']
        );
    }

}
