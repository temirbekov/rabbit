<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiBaseController;
use App\Repositories\CrmMessageRepository;
use App\Repositories\EmailRepository;

class MainController extends ApiBaseController
{
    public function generate(EmailRepository $emailRepository, CrmMessageRepository $crmMessageRepository)
    {
        if(rand(0,1)) {
            $model = factory(\App\CrmMessage::class)->create();
            $crmMessageRepository->dispatch($model);
        } else {
            $model = factory(\App\Email::class)->create();
            $emailRepository->dispatch($model);
        }

        return $this->sendResponse(
            true, ['status'=>'success']
        );
    }
}
