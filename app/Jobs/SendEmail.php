<?php

namespace App\Jobs;

use App\Email;
use App\Repositories\QueueRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;

    public function __construct(Email $email)
    {
        $this->email = $email;
    }

    public function handle(QueueRepository $queueRepository)
    {
        //Представим что тут идет отправка E-mail
        $this->email->job_status = true;
        $this->email->save();

        //Отправляем сообщение что выполнили задачу в Вебсокет сервис через очередь
        $queueRepository->sendBroadcast($this->email->subject,'email',$this->email->message);
    }
}
