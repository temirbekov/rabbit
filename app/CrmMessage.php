<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrmMessage extends Model
{
    protected $fillable = [
        'title', 'message',
    ];
}
