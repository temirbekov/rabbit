
# Тестовое задание:  #

Написать на любом (но не доисторическом) фреймворке сервис, который будет слушать очередь и пересылать сообщения из неё !всем клиентам! (т.е. не нужно заморачиваться с адресатами и всем прочим, просто пишем транслятор) в соединения web-socket.   
Также написать простейший клиент и генератор задач в очередь.   
Брокер очередей можно выбрать любой но желательно RabbitMq.   
Желательно оформить сервис как unit-service linux и описать доку как им пользоваться.   
Примечание, использование фремйворка желательно, но не обязательно..  

##### Backend #####
- Laravel 5.6
- RabbitMQ
- GoLang

##### FrontEnd:  #####
- Vue.js + Vuetify

#### Для запуска нужно скачать проект ####

git clone https://gitlab.com/temirbekov/rabbit

#### Запуск среды ####

cd laradock  
cp .env-lararabbit .env  
docker-compose up -d nginx mysql phpmyadmin redis workspace rabbitmq  

После того как все настроится, нужно зайти в консоль рабочей среды:  
docker-compose exec workspace bash  

#### Установка проекта и запуск проекта ####

cp env-rabbit .env  
composer install  
npm install  
php artisan migrate:refresh  
npm run dev  
php artisan queue:work --queue=email,crm_message  


#### Транслятор на GO ####

//Скачиваем проект и пакеты к нему  
git clone https://gitlab.com/temirbekov/rabbit_go  
cd rabbit_go  
go get github.com/gorilla/websocket  
go get github.com/streadway/amqp  

//Компилируем  
build  

//Запускаем с параметром --server где указывается наш RabbitMQ, сервис слушает очередь с именем "broadcast"  
./rabbit_go --server amqp://guest:guest@localhost:5672/  

